module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
  ? '/wp-content/themes/ilrus/new-site/'
  : '/',
  devServer: {
    proxy: 'http://ilrusstroy.ru'
  },
}