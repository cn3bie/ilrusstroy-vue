import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    }

    return { x: 0, y: 0 };
  },
  routes: [{
    path: '/',
    name: 'home',
    meta: {
      title: 'Строительство загородных домов и коттеджей в Санкт-Петербурге и Ленинградской области - Илрусстрой'
    },
    component: () => import(/* webpackChunkName: "about" */ './views/home/home.component.vue'),
  }, {
    path: '/about',
    name: 'about',
    meta: {
      title: 'Архитектурно-строительная компания "Илрусстрой". Проектируем и строим загородные дома, коттеджи <br />в Санкт-Петербурге и Ленинградской области с 2008 года'
    },
    component: () => import(/* webpackChunkName: "about" */ './views/about/about.component.vue'),
  }, {
    path: '/catalog',
    meta: {
      title: 'Проекты домов и цены'
    },
    alias: [
      '/catalog/gaz',
      '/catalog/kirpich',
      '/catalog/keramzit',
      '/catalog/pen',
      '/catalog/onefloor',
      '/catalog/onefloor_plus_mansandra',
      '/catalog/twofloor',
      '/catalog/twofloor_plus_mansandra',
    ],
    component: () => import(/* webpackChunkName: "catalog" */ './views/catalog/catalog.component.vue'),
  }, {
    path: '/catalog/:id',
    name: 'catalog_card',
    component: () => import(/* webpackChunkName: "cardCatalog" */ './views/catalog/card/catalog-card.component.vue'),
  }, {
    path: '/services',
    name: 'services',
    meta: {
      title: 'Услуги'
    },
    component: () => import(/* webpackChunkName: "services" */ './views/services/services.component.vue'),
  }, {
    path: '/news',
    name: 'news',
    meta: {
      title: 'Полезные статьи <br />о строительстве'
    },
    component: () => import(/* webpackChunkName: "news" */ './views/news/news.component.vue'),
  }, {
    path: '/news/:id',
    name: 'news_card',
    component: () => import(/* webpackChunkName: "news" */ './views/news/card/news-card.component.vue'),
  }, {
    path: '/reviews',
    name: 'reviews',
    meta: {
      title: 'Посмотрите, что говорят о нас клиенты'
    },
    component: () => import(/* webpackChunkName: "reviews" */ './views/reviews/reviews.component.vue'),
  }, {
    path: '/contacts',
    name: 'contacts',
    meta: {
      title: 'Контакты'
    },
    component: () => import(/* webpackChunkName: "contacts" */ './views/contacts/contacts.component.vue'),
  }, {
    path: '/build',
    name: 'build',
    meta: {
      title: 'Дома строятся'
    },
    component: () => import(/* webpackChunkName: "build" */ './views/build/build.component.vue'),
  }, {
    path: '/build/:id',
    name: 'build_card',
    component: () => import(/* webpackChunkName: "cardBuild" */ './views/build/card/build-card.component.vue'),
  }, {
    path: '/builded',
    name: 'builded',
    meta: {
      title: 'Дома построены'
    },
    component: () => import(/* webpackChunkName: "builded" */ './views/builded/builded.component.vue'),
  }, {
    path: '/builded/:id',
    name: 'builded_card',
    component: () => import(/* webpackChunkName: "cardBuild" */ './views/builded/card/builded-card.component.vue'),
  }, {
    path: '/thanks',
    name: 'thanks',
    meta: {
      title: 'Спасибо, что обратились в нашу строительную компанию'
    },
    component: () => import(/* webpackChunkName: "thanks" */ './views/thanks/thanks.component.vue'),
  }, {
    path: '/bookmark',
    name: 'bookmark',
    meta: {
      title: 'Избранное'
    },
    component: () => import(/* webpackChunkName: "bookmark" */ './views/bookmark/bookmark.component.vue'),
  }, {
    path: '*',
    meta: {
      title: 'Страница не найдена'
    },
    component: () => import(/* webpackChunkName: "notFound" */ './views/not-found/not-found.component.vue'),
  }],
});
