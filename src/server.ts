const express = require('express');

const path = require('path');

const app = express();

app.use(express.static('/'));

app.get('/', (req: any, res: any) => {
    res.sendFile(path.resolve('/', 'index.html'));
});

app.listen(3000, () => {
    console.log('\n\nApp on port 3000!\n\n');
});
