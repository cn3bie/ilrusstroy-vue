import {IArticle} from '@/models';

export const ListArticles: IArticle[] = [];

for (let i = 15; i--;) {
  ListArticles.push({
    title: `Title article ${i}`,
    img: require('@/assets/card.jpg'),
    desc: 'Это очень длинный текст для проверки на сгибание нервов',
  });
}
