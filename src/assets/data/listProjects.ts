import {
  IProject,
  TypeMaterial,
  TypeConstruction,
  TypeInput,
  ICn3bieFilter,
  IPhotootchet,
  IImgWithName,
  getRandom
} from '@/models';
import {TypeName} from '@/filters';

export const ListProjects: IProject[] = [];

export const ListTypeMaterial: TypeMaterial[] = [
  TypeMaterial.gaz,
  TypeMaterial.kirpich,
  TypeMaterial.keramzit,
  TypeMaterial.pen,
];

export const ListPriceByTypeMaterial: Map<TypeMaterial, number> = new Map<TypeMaterial, number>([
  [TypeMaterial.gaz, 18500],
  [TypeMaterial.kirpich, 21500],
  [TypeMaterial.keramzit, 19000],
  [TypeMaterial.pen, 18000],
]);

function createProject(id: string): IProject {
  return {
    id,
    name: `Шевалье ${id}`,
    img: `https://picsum.photos/1600/900/?random=${id}`,
    url: 'card',
    additional: {},
    mansarda: false,
    metadata: {
      index: 0,
      bookmark: getRandom(0, 10) > 5,
      open: getRandom(0, 10) > 5,
      snackbar: {
        status: false,
      },
    },
    area: getRandom(110, 300),
    plans: [{
      name: 'Name 1',
      area: [['str', getRandom(110, 300)]],
      img: `https://picsum.photos/1600/900/?random=${id + 1}`,
    }, {
      name: 'Name 2',
      area: [['str', getRandom(110, 300)]],
      img: `https://picsum.photos/1600/900/?random=${id + 2}`,
    }],
    material: ListTypeMaterial[getRandom(0, ListTypeMaterial.length - 1)],
    countBedroom: getRandom(1, 3),
    countFloor: getRandom(1, 3),
    price: getRandom(230000, 3000000),
    address: `г. Санкт-Петербург, м. Ладожская, ул. Магнитогорская 51а ${id}`,
    complect: {},
  };
}

function createHouse(id: string): IProject {
  return {
    id,
    name: `Шевалье ${id}`,
    img: `https://picsum.photos/1600/900/?random=${id}`,
    url: `/build/${id}`,
    additional: {},
    mansarda: false,
    metadata: {
      index: 0,
      bookmark: getRandom(0, 10) > 5,
      open: getRandom(0, 10) > 5,
      snackbar: {
        status: false,
      },
    },
    area: getRandom(110, 300),
    plans: [{
      name: 'Name 1',
      area: [['str', getRandom(110, 300)]],
      img: `https://picsum.photos/1600/900/?random=${id + 1}`,
    }, {
      name: 'Name 2',
      area: [['str', getRandom(110, 300)]],
      img: `https://picsum.photos/1600/900/?random=${id + 2}`,
    }],
    material: ListTypeMaterial[getRandom(0, ListTypeMaterial.length - 1)],
    countBedroom: getRandom(1, 3),
    countFloor: getRandom(1, 3),
    price: getRandom(230000, 3000000),
    address: `г. Санкт-Петербург, м. Ладожская, ул. Магнитогорская 51а ${id}`,
    complect: {},
  };
}

export const EmptyProject: IProject = {
  id: '',
  name: '',
  img: '',
  url: '',
  additional: {},
  mansarda: false,
  metadata: {
    index: 0,
    bookmark: false,
    open: false,
    snackbar: {
      status: false,
    },
  },
  area: 0,
  plans: [],
  material: ListTypeMaterial[getRandom(0, ListTypeMaterial.length - 1)],
  countBedroom: 0,
  countFloor: 0,
  price: 0,
  address: '',
  complect: {},
};

// for (let i = 256; i--;) {
//   ListProjects.push(createProject(('' + i)));
// }
for (let i = 256; i--;) {
  ListProjects.push(createHouse(('' + i)));
}

export const ListAdditionals: {
  [key: string]: IImgWithName;
} = {
  'garazh': {
    name: 'Гараж',
    img: require('@/assets/icon/parking.jpg'),
  },
  'spa': {
    name: 'СПА',
    img: require('@/assets/icon/meditation.svg'),
  },
  'baseyn': {
    name: 'Басейн',
    img: require('@/assets/icon/pool.svg'),
  },
  'sauna': {
    name: 'Сауна',
    img: require('@/assets/icon/towel.svg'),
  },
  'trenazhorniy-zal': {
    name: 'Тренировочный зал',
    img: require('@/assets/icon/bicycle.svg'),
  },
};

export const ListFasads: string[] = [];
const listList: string[] = [
  'Кровля : металлочерепица',
  'Фасад : облицовочный кирпич ЛСР, цвет какаха',
  'Перекрытие 1 этажа : монолитное',
  'Перекрытие 2 этажа : деревянное',
  'Высота потолка 1 этажа : 3м',
  'Высота потолка 2 этажа : 2,7м',
];
export const ListPhotootchet: IPhotootchet[] = [];

for (let i = 4; i--;) {
  const item: IPhotootchet = {
    id: '',
    address: '',
    name: `Построенный дом №${i}`,
    list: listList.map((str: string) => str + ' #' + i),
    images: [],
  };

  for (let j = 4; j--;) {
    item.images.push(`https://picsum.photos/1600/900/?random=${j + '-' + i}`);
  }

  ListPhotootchet.push(item);
}

for (let i = 4; i--;) {
  ListFasads.push(`https://picsum.photos/1600/900/?random=${i}`);
}

const text = `
  Разметка осей фундамента с установкой колушков
  Бетон В22, 5 М300
  Устройство опалубки цоколя:
  Доска обрезная 150х50х6000 мм
  Гвозди строительные 100м
  Устройство арматурного каркаса:
  Арматура d - 12 АIII
  Вязальная проволока
  Доставка
  Расходные материалы
  Автобетононасос
  Укладка геотекстиля
`;

export const complectAdditionals = [{
  name: 'Доставка',
  img: require('@/assets/icon/delivery.svg'),
}, {
  name: 'Расходные материалы',
  img: require('@/assets/icon/replace.svg'),
}, {
  name: 'Бытовка и организация проживания бригады',
  img: require('@/assets/icon/people.svg'),
}, {
  name: 'Геологические изычкания',
  img: require('@/assets/icon/eye.svg'),
}, {
  name: 'Проект дома',
  img: require('@/assets/icon/house.svg'),
}, {
  name: 'Привязка дома к участку',
  img: require('@/assets/icon/flag.svg'),
}];

export const ListTypeConstruction = {
  [TypeConstruction.box]: text,
  [TypeConstruction.clearing]: text,
};

export const FilterPrice: ICn3bieFilter = {
  title: 'Цена',
  name: 'price',
  slider: {
    label: '<span>₽</span>',
    min: 100000,
    max: 10000000,
    range: [500000, 1000000],
  },
};
export const FilterArea: ICn3bieFilter = {
  title: 'Общая площадь',
  name: 'area',
  slider: {
    label: '<span class="pow">m</span>',
    min: 0,
    max: 300,
    range: [24, 224],
  },
};

export const FilterLength: ICn3bieFilter = {
  title: 'Длина',
  name: 'length',
  slider: {
    label: '<span class="pow">m</span>',
    min: 0,
    max: 100,
    range: [9, 36],
  },
};

export const FilterWidth: ICn3bieFilter = {
  title: 'Ширина',
  name: 'width',
  slider: {
    label: '<span class="pow">m</span>',
    min: 0,
    max: 100,
    range: [5, 18],
  },
};

export const FilterMaterial: ICn3bieFilter = {
  title: 'Материал стен',
  name: 'material',
  box: {
    type: TypeInput.radio,
    list: ListTypeMaterial.map((material: TypeMaterial) => ({
      label: TypeName(material),
      value: material,
      checked: false,
    })),
  },
};

export const ListFilters: ICn3bieFilter[] = [FilterPrice, FilterArea, FilterMaterial, {
    title: 'Этажность',
    name: 'floor',
    box: {
      type: TypeInput.checkbox,
      list: [
        { label: '1 этаж', value: '1', checked: false },
        { label: '1 этаж + мансарда', value: '3', checked: false },
        { label: '2 этаж', value: '2', checked: false },
        { label: '2 этаж + мансарда', value: '4', checked: false },
        { label: '2 этаж + цокольный этаж', value: '5', checked: false },
      ],
    },
  }, {
    title: 'Количество спален',
    name: 'bedroom',
    box: {
      type: TypeInput.checkbox,
      list: [
        { label: '1', value: '1', checked: false },
        { label: '2', value: '2', checked: false },
        { label: '3', value: '3', checked: false },
        { label: '4', value: '4', checked: false },
        { label: '5', value: '5', checked: false },
        { label: '6+', value: '6+', checked: false },
      ],
    },
  }, {
    title: 'Дополнительно',
    name: 'additional',
    box: {
      type: TypeInput.checkbox,
      list: [
        { label: 'Терраса', value: 'terrasa', checked: false },
        { label: 'Балкон', value: 'balkon', checked: false },
        { label: 'Бассейн', value: 'baseyn', checked: false },
        { label: 'Веранда', value: 'veranda', checked: false },
        { label: 'Второй свет', value: 'two-svet', checked: false },
        { label: 'Гараж', value: 'garazh', checked: false },
        { label: 'Навес для автомобиля', value: 'naves-dlya-auto', checked: false },
        { label: 'Сауна', value: 'sauna', checked: false },
        { label: 'СПА зона', value: 'spa', checked: false },
        { label: 'Тренажерный зал', value: 'trenazhorniy-zal', checked: false },
        { label: 'Эркер', value: 'erker', checked: false },
      ],
    },
  }, FilterLength, FilterWidth];

export default ListProjects;
