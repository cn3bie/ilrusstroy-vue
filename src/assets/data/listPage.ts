export interface IItemPage {
  title: string;
  link?: string;
  count: number;
  children?: IItemPage[];
}

export const ListPage: IItemPage[] = [{
  title: 'Проекты и цены',
  link: '/catalog/',
  count: 0,
  children: [{
    title: 'Все проекты',
    link: '/catalog/',
    count: 0,
  }, {
    title: 'Технология',
    count: 0,
    children: [{
      title: 'Дома из газобетона',
      link: '/catalog/gaz/',
      count: 0,
    }, {
      title: 'Дома из кирпича',
      link: '/catalog/kirpich/',
      count: 0,
    }, {
      title: 'Дома из керамзитобетона',
      link: '/catalog/keramzit/',
      count: 0,
    }, {
      title: 'Дома из пенобетона',
      link: '/catalog/pen/',
      count: 0,
    }],
  }, {
    title: 'Этажность',
    count: 0,
    children: [{
      title: 'Один этаж',
      link: '/catalog/onefloor/',
      count: 0,
    }, {
      title: 'Один этаж + мансанрда',
      link: '/catalog/onefloor_plus_mansandra/',
      count: 0,
    }, {
      title: 'Два этажа',
      link: '/catalog/twofloor/',
      count: 0,
    }, {
      title: 'Два этажа + мансанрда',
      link: '/catalog/twofloor_plus_mansandra/',
      count: 0,
    }],
  }],
}, {
  title: 'Дома строятся',
  link: '/build/',
  count: 1,
}, {
  title: 'Дома построенные',
  link: '/builded/',
  count: 0,
}, {
  title: 'Услуги',
  link: '/services/',
  count: 0,
}, {
  title: 'Отзывы',
  link: '/reviews/',
  count: 0,
}, {
  title: 'О нас',
  link: '/about/',
  count: 0,
}, {
  title: 'Статьи',
  link: '/news/',
  count: 0,
}, {
  title: 'Контакты',
  link: '/contacts/',
  count: 0,
}];

export const FooterMenu: IItemPage[] = [{
  title: 'Проекты и цены',
  link: '/catalog/',
  count: 0,
  children: [{
    title: 'Дома из газобетона',
    link: '/catalog/gaz/',
    count: 0,
  }, {
    title: 'Дома из кирпича',
    link: '/catalog/kirpich/',
    count: 0,
  }, {
    title: 'Дома из керамзитобетона',
    link: '/catalog/keramzit/',
    count: 0,
  }, {
    title: 'Дома из пенобетона',
    link: '/catalog/pena/',
    count: 0,
  }],
}, {
  title: 'Дома строятся',
  link: '/build/',
  count: 0,
}, {
  title: 'Дома построенные',
  link: '/builded/',
  count: 0,
}, {
  title: 'Услуги',
  link: '/services/',
  count: 0,
}, {
  title: 'Отзывы',
  link: '/reviews/',
  count: 0,
}, {
  title: 'О нас',
  link: '/about/',
  count: 0,
}, {
  title: 'Новости',
  link: '/news/',
  count: 0,
}, {
  title: 'Контакты',
  link: '/contacts/',
  count: 0,
}];

export default ListPage;
