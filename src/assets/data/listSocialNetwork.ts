export interface IItemSocialNetworkl {
  name: string;
  img: string;
  icon: string;
  link: string;
}

export const ListSocialNetwork: IItemSocialNetworkl[] = [{
  name: 'Facebook',
  img: require('@/assets/socialnetwork/facebook.svg'),
  icon: 'fab fa-facebook-f',
  link: 'facebook.com',
}, {
  name: 'Vkontakte',
  img: require('@/assets/socialnetwork/vk.svg'),
  icon: 'fab fa-vk',
  link: 'vk.com',
}, {
  name: 'Ok',
  img: require('@/assets/socialnetwork/ok.svg'),
  icon: 'fab fa-odnoklassniki',
  link: 'odnoklassniki.com',
}, {
  name: 'Instagram',
  img: require('@/assets/socialnetwork/instagram.svg'),
  icon: 'fab fa-instagram',
  link: 'instagram.com',
}];

export default ListSocialNetwork;
