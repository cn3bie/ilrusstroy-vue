import {IReview, IReviewVideo, getRandom, longText} from '@/models';

export const ListReviews: IReview[] = [];
export const ListReviewsVideo: IReviewVideo[] = [];

for (let i = 3; i--;) {
  ListReviews.push({
    name: `Шевалье ${i}`,
    url: 'card',
    text: longText,
    img: `https://picsum.photos/700/800/?random=${i}`,
    imgFull: `https://picsum.photos/700/800/?random=${i}`,
  });
}

for (let i = 2; i--;) {
  ListReviewsVideo.push({
    name: `Используя lorem ifsdf для распечатки образцов ${i}.`,
    url: 'card',
    img: `https://picsum.photos/700/800/?random=${i}`,
    play: false,
    time: '2:30',
  });
}

export default ListReviews;
