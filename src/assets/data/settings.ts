import ListPhone from '@/assets/data/listPhone';

export const Settings = {
  title: 'Ilrusstroy',
  address: 'г. Санкт-Петербург, м. Ладожская, ул. Магнитогорская 51а',
  phone: ListPhone[0].phone,
  email: 'info@ilrustroy.ru',
  time: 'Пн.-Пт. 10:00-20:00',
};

export default Settings;
