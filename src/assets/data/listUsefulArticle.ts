import {IUsefulArticle, longText} from '@/models';

export const ListUsefulArticle: IUsefulArticle[] = [];

for (let i = 4; i--;) {
  ListUsefulArticle.push({
    name: 'Шевалье',
    url: 'card',
    text: longText,
    img: `https://picsum.photos/700/800/?random=${i}`,
  });
}

export default ListUsefulArticle;
