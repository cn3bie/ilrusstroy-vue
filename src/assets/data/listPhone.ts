export interface IItemPhone {
  phone: string;
  text: string;
}

export const ListPhone: IItemPhone[] = [{
  phone: '+7 (812) 920-25-76',
  text: 'Санкт-Петербург',
}, {
  phone: '+7 (812) 957-23-24',
  text: 'Для регионов',
}];

export default ListPhone;
