import {IComplect} from './projects.model';

export interface IBetween {
  min: number;
  max: number;
}

export interface IListBookmark {
  [key: string]: number;
}

export interface IResponseModel<T> {
  totalCount: number;
  data: T[];
  settings: {
    [key: string]: IBetween;
  };
  filterCount: number;
}

export interface IResponseBookmark {
  totalCount: number;
  data: IListBookmark;
  id: null | number;
}

export interface IResponseSettingsProject {
  gaz: IComplect;
  kirpich: IComplect;
  keramzit: IComplect;
  pen: IComplect;
}

export interface IResponseError {
  [key: string]: boolean;
}
