export interface IImgWithName {
  name: string;
  img: string;
}

export function getRandom(min: number, max: number): number {
  return Math.floor(Math.random() * (max - min)) + min;
}

// tslint:disable max-line-length
export const longText = 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Exercitationem repellendus quae saepe rem, ducimus fugiat, eaque eum aspernatur doloribus esse vel qui, aut dicta nihil inventore totam! Doloremque facere debitis repellendus.';

export interface IViewer {
  show: (i: number) => void;
}
