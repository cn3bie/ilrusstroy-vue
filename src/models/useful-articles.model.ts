export interface IUsefulArticle {
  name: string;
  url: string;
  text: string;
  img: string;
}
