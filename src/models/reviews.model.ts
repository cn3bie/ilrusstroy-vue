export interface IReview {
  name: string;
  url: string;
  text: string;
  img: string;
  imgFull: string;
}
export interface IReviewVideo {
  name: string;
  url: string;
  img: string;
  play: boolean;
  time: string;
}
