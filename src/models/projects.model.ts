import {IImgWithName} from './shared.model';

export interface IImg {
  img: string;
  imgFull: string;
}

export enum TypeMaterial {
  gaz = 'gaz',
  kirpich = 'kirpich',
  keramzit = 'keramzit',
  pen = 'pen',
}

export enum TypeInput {
  text = 'text',
  radio = 'radio',
  checkbox = 'checkbox',
  file = 'file',
}

export enum TypeConstruction {
  box = 'box',
  clearing = 'clearing',
}

export interface IPlan {
  name: string;
  img: string;
  area: Array<[string, number]>;
}

export interface IProject extends IImgWithName {
  id: string;
  area: number;
  countFloor: number;
  countBedroom: number;
  price: number;
  url: string;
  additional: {
    [key: string]: string;
  };
  mansarda: boolean;
  metadata: {
    index: number;
    bookmark: boolean;
    open: boolean;
    snackbar: {
      status: boolean;
      timeout?: any;
    };
  };
  plans: IPlan[];
  material: TypeMaterial;
  address: string;
  hasProjects?: boolean;
  complect: {
    [key: string]: IComplect;
  };
}

export interface IResponseProject extends IProject {
  images: string[];
  fasads: string[];
}

export interface IResponseProjectGallery {
  id: string;
  data: IPhotootchet[];
}

export interface IComplect {
  box: string;
  clearing: string;
}

export interface IPhotootchet {
  id: string;
  name: string;
  address: string;
  images: string[];
  list: string[];
}

export interface IInputBox {
  label: string;
  value: string;
  checked: boolean;
}

export interface IBox {
  type: TypeInput;
  list: IInputBox[];
}

export interface ICn3bieSlider {
  label: string;
  min: number;
  max: number;
  range: [number, number];
}

export interface ICn3bieFilter {
  title: string;
  name: string;
  slider?: ICn3bieSlider;
  box?: IBox;
}

export interface ISort {
  order: string;
  orderby: string;
}

export interface ICn3bieSort {
  name: string;
  value: ISort;
}

export type SimpleType = string | number | boolean;

export interface IParam {
  name: string;
  value: SimpleType;
  type?: TypeInput;
}

export interface IGetParams {
  [key: string]: SimpleType | SimpleType[];
}

export interface IQueryFilter {
  name: string;
  params: IParam[];
}


export enum TypeTarget {
  box = 'box',
  slider = 'slider',
}

export interface IResponsFilterItem {
  query: IQueryFilter;
  event: Event | undefined;
  target?: {
    el: Element;
    type: TypeTarget;
  };
}
