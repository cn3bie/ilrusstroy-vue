export interface IPerson {
  id?: string | number;
  name: string;
  order: number;
  text: string;
  position: string;
  callManager: boolean;
  timeManagment?: string;
  img: string;
  imgCrop: string;
  imgCropBW: string;
  size?: number;
}
