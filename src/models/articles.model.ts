
export interface IArticle {
  title: string;
  img: string;
  desc: string;
}
