import '@babel/polyfill';
import Vue from 'vue';
import './plugins/vuetify';

import App from './App.vue';
import router from './router';
import store from './store';

router.beforeEach((to, from, next) => {
  if (to.meta.title) {
    document.title = to.meta.title;
  }

  next();
});

import 'roboto-fontface/css/roboto/roboto-fontface.css';
import 'material-design-icons-iconfont/dist/material-design-icons.css';

import VueAwesomeSwiper from 'vue-awesome-swiper';
import 'swiper/dist/css/swiper.css';

import '@/assets/fonts/fonts.sass';
import '@/assets/icon/icon.scss';

import {TypeName, FormatPrice, ComplectName} from '@/filters';


import 'viewerjs/dist/viewer.css';
import Viewer from 'v-viewer';
Vue.use(Viewer);

Vue.use(VueAwesomeSwiper);

Vue.filter('TypeName', TypeName);
Vue.filter('FormatPrice', FormatPrice);
Vue.filter('ComplectName', ComplectName);

import VueYouTubeEmbed from 'vue-youtube-embed';

Vue.use(VueYouTubeEmbed, { global: true, componentId: 'youtube-media' });

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h: any) => h(App),
}).$mount('#app');
