import Vue from 'vue';
import Vuex from 'vuex';
import {AxiosResponse} from 'axios';
import {IResponseModel, IPerson, IArticle, IReview, IBetween, IResponseSettingsProject, IComplect} from '../models';

import {IProject, IPlan, TypeMaterial, IInputBox} from '../models/projects.model';

import {
  EmptyProject,
  FilterPrice,
  FilterArea,
  FilterLength,
  FilterWidth,
  FilterMaterial,
} from '../assets/data/listProjects';

import projectsServer from '@/services/projects.service';
import commonServer from '@/services/common.service';
import listPerson from '../assets/data/listPerson';

interface ICollection {
  projects: IProject[];
  size: number;
}

Vue.use(Vuex);
interface IRootState {
  projects: Map<string, IProject>;
  bookmark: {
    ids: {
      [key: string]: IProject;
  },
    count: number;
    projects: IProject[];
  };
  open: {
    ids: {
      [key: string]: IProject;
    },
    count: number;
    projects: IProject[];
  };
  catalog: {
    currentMatterial: TypeMaterial;
    projects: IProject[];
    size: number;
    filterCount: number;
    totalCount: number;
    moreCount: number;
    asLine: boolean;
    priceFilter: {
      [key: string]: IBetween;
    };
  };
  other: {
    projects: IProject[];
    size: number;
  };
  popular: IProject[];
  build: ICollection;
  builded: ICollection;
  currentProject: {
    project: IProject;
    images: string[];
    address: string;
    plans: IPlan[];
    fasads: string[];
  };
  emploees: {
    active: IPerson;
    index: number;
    list: IPerson[];
    managers: IPerson[];
  };
  news: IArticle[];
  reviews: IReview[];
  settingsComplect: {
    [key: string]: IComplect;
  };
}

const managers = listPerson.filter((item: IPerson) => item.callManager);

export interface IAction {
  type: string;
  payload?: any;
}


const AppRootState: IRootState = {
  projects: new Map<string, IProject>(),
  bookmark: {
    ids: {},
    count: 0,
    projects: [],
  },
  open: {
    ids: {},
    count: 0,
    projects: [],
  },
  catalog: {
    currentMatterial: TypeMaterial.gaz,
    projects: [],
    size: 18,
    filterCount: 0,
    totalCount: 18,
    moreCount: 0,
    asLine: false,
    priceFilter: {},
  },
  other: {
    projects: [],
    size: 4,
  },
  popular: [],
  build: {
    projects: [],
    size: 20,
  },
  builded: {
    projects: [],
    size: 20,
  },
  currentProject: {
    project: EmptyProject,
    images: [],
    plans: [],
    fasads: [],
    address: '',
  },
  emploees: {
    managers,
    index: 1,
    active: managers[1],
    list: listPerson,
  },
  news: [],
  reviews: [],
  settingsComplect: {
    [TypeMaterial.gaz]: {
      box: '',
      clearing: '',
    },
  },
};

function getProject(state: IRootState, project: IProject): IProject {
  if (!state.projects.has(project.id)) {
    state.projects.set(project.id, project);
  }

  return state.projects.get(project.id) || project;
}

function udpateStateForProjects(projects: IProject[], state: IRootState): IProject[] {
  return projects.map((project: IProject) => {
    project = getProject(state, project);

    if (state.bookmark.ids[project.id]) {
      project.metadata.bookmark = true;
    } else if (project.metadata.bookmark) {
      state.bookmark.ids[project.id] = project;
      state.bookmark.projects.push(project);

      state.bookmark.count = Object.keys(state.bookmark.ids).length;
    }

    if (state.open.ids[project.id]) {
      project.metadata.open = true;
    }

    return project;
  });
}

export enum ActionType {
  ToggleBookmark = 'toggleBookmark',
  AddOpen = 'addOpen',
  RemoveOpen = 'removeOpen',
  LoadedProjects = 'loadedProjects',
  LoadedPopularProjects = 'loadedPopularProjects',
  LoadedOtherProjects = 'loadedOtherProjects',
  LoadedBuildProjects = 'loadedBuildProjects',
  LoadedBuildedProjects = 'loadedBuildedProjects',
  LoadedBookmarkProjects = 'loadedBookmarkProjects',
  GridAsLine = 'gridAsLine',
  SelectedProject = 'selectedProject',
  TotalCount = 'totalCount',
  MoreCount = 'moreCount',
  FilterCount = 'filterCount',
  ChangePerson = 'changePerson',
  ChangeMatterial = 'changeMatterial',
  LoadedNews = 'loadedNews',
  LoadedReviews = 'loadedReviews',
  ChangeBetweenPriceMatterial = 'changeBetweenPriceMatterial',
  LoadedPriceFilter = 'loadedPriceFilter',
  LoadedSettingsComplect = 'loadedSettingsComplect',
  ChangeImagesSelectedProject = 'changeImagesSelectedProject',
}

export default new Vuex.Store<IRootState>({
  state: AppRootState,
  mutations: {
    [ActionType.ToggleBookmark](state, project: IProject) {
      project = getProject(state, project);
      const oldValue = project.metadata.bookmark;

      project.metadata.bookmark = !oldValue;

      if (!oldValue) {
        state.bookmark.ids[project.id] = project;
        state.bookmark.projects.push(project);
      } else {
        delete state.bookmark.ids[project.id];
        const index = state.bookmark.projects.findIndex((proj: IProject) => proj.id === project.id);

        state.bookmark.projects.splice(index, 1);
      }

      state.bookmark.count = Object.keys(state.bookmark.ids).length;
    },
    [ActionType.AddOpen](state, project: IProject) {
      project = getProject(state, project);

      state.open.ids[project.id] = project;
      state.open.ids[project.id].metadata.open = true;
      state.open.count = Object.keys(state.open.ids).length;
      state.open.projects.push(project);
    },
    [ActionType.RemoveOpen](state, project: IProject) {
      project = getProject(state, project);

      state.open.ids[project.id].metadata.open = false;
      delete state.open.ids[project.id];
      state.open.count = Object.keys(state.open.ids).length;

      state.open.projects.splice(state.open.projects.findIndex((proj: IProject) => proj.id === project.id), 1);
    },
    [ActionType.LoadedProjects](state, collection: ICollection) {
      state.catalog.projects.length = 0;

      if (collection.size) {
        state.catalog.size = collection.size;
      }

      state.catalog.moreCount = state.catalog.totalCount - collection.projects.length;

      state.catalog.projects.push(...udpateStateForProjects(collection.projects, state));
    },
    [ActionType.LoadedPopularProjects](state, projects: IProject[]) {
      state.popular.length = 0;
      state.popular.push(...udpateStateForProjects(projects, state));
    },
    [ActionType.LoadedOtherProjects](state, collection: ICollection) {
      state.other.projects.length = 0;

      if (collection.size) {
        state.other.size = collection.size;
      }

      state.other.projects.push(...udpateStateForProjects(collection.projects, state));
    },
    [ActionType.LoadedBuildProjects](state, collection: ICollection) {
      state.build.projects.length = 0;

      if (collection.size) {
        state.build.size = collection.size;
      }

      state.build.projects.push(...collection.projects);
    },
    [ActionType.LoadedBuildedProjects](state, collection: ICollection) {
      state.build.projects.length = 0;

      if (collection.size) {
        state.build.size = collection.size;
      }

      state.builded.projects.push(...collection.projects);
    },
    [ActionType.LoadedBookmarkProjects](state, projects: IProject[]) {
      state.bookmark.projects.length = 0;
      state.bookmark.projects.push(...udpateStateForProjects(projects, state));
    },
    [ActionType.GridAsLine](state, value: boolean) {
      state.catalog.asLine = value;
    },
    [ActionType.SelectedProject](state, project: IProject) {
      state.currentProject.project = project;
    },
    [ActionType.ChangeImagesSelectedProject](state, images: string[]) {
      state.currentProject.images = images;
    },
    [ActionType.TotalCount](state, count: number) {
      state.catalog.totalCount = count;
    },
    [ActionType.MoreCount](state, count: number) {
      state.catalog.moreCount = count;
    },
    [ActionType.FilterCount](state, count: number) {
      state.catalog.filterCount = count;
    },
    [ActionType.ChangePerson](state, pers: IPerson) {
      state.emploees.active = pers;
      state.emploees.index = state.emploees.managers.findIndex((manager: IPerson) => manager === pers);
    },
    [ActionType.ChangeMatterial](state, matterial: TypeMaterial) {
      state.catalog.currentMatterial = matterial;

      if (FilterMaterial.box) {
        FilterMaterial.box.list.map((item: IInputBox) => {
          item.checked = item.value === matterial;

          return item;
        });
      }
    },
    [ActionType.LoadedPriceFilter](state, priceList: {[key: string]: IBetween}) {
      state.catalog.priceFilter = priceList;
    },
    [ActionType.ChangeBetweenPriceMatterial](state, matterial: TypeMaterial) {
      projectsServer.getBetween({
        price: state.catalog.priceFilter[matterial],
      }, [FilterPrice]);
    },
    [ActionType.LoadedNews](state, articles: IArticle[]) {
      state.news.length = 0;

      state.news.push(...articles);
    },
    [ActionType.LoadedNews](state, articles: IArticle[]) {
      state.news.length = 0;

      state.news.push(...articles);
    },
    [ActionType.LoadedReviews](state, reviews: IReview[]) {
      state.reviews.length = 0;

      state.reviews.push(...reviews);
    },
    [ActionType.LoadedSettingsComplect](state, settingsComplect: IResponseSettingsProject) {
      Object.assign(state.settingsComplect, settingsComplect);
    },
  },
  actions: {
    loadRoot(store) {
      this.commit(ActionType.ChangeMatterial, TypeMaterial.gaz);

      projectsServer.getSettingProjects()
        .then(res => {
          store.commit(ActionType.TotalCount, res.data.totalCount);

          projectsServer.getBetween(res.data.settings, [
            FilterArea,
            FilterLength,
            FilterWidth,
          ]);

          if (res.data.settings.price) {
            store.commit(ActionType.LoadedPriceFilter, res.data.settings.price);
            store.commit(ActionType.ChangeBetweenPriceMatterial, store.state.catalog.currentMatterial);
          }
        })
        .then(() => {
          projectsServer.getProjects(1, store.state.catalog.size, 'ASC', 'price')
            .then(res => {
              store.commit(ActionType.LoadedProjects, {projects: res.data.data as IProject[] });
              store.commit(ActionType.FilterCount, store.state.catalog.totalCount);
              store.commit(ActionType.MoreCount, store.state.catalog.totalCount - res.data.data.length);
            });
        });

      projectsServer.getPopularProjects()
        .then(res => {
          store.commit(ActionType.LoadedPopularProjects, res.data.data as IProject[]);
        });

      commonServer.getProjectSettings()
        .then((res: AxiosResponse<IResponseSettingsProject>) => {
          store.commit(ActionType.LoadedSettingsComplect, res.data);
        });
    },
    [ActionType.ChangeMatterial](store, matterial) {
      store.commit(ActionType.ChangeMatterial, matterial);
      store.commit(ActionType.ChangeBetweenPriceMatterial, store.state.catalog.currentMatterial);
    },
    [ActionType.ToggleBookmark](store, project: IProject) {
      store.commit(ActionType.ToggleBookmark, project);
      projectsServer.toggleBookmarkProjectById(project.id);
    },
  },
});
