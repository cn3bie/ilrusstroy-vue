import {TypeMaterial, TypeConstruction} from '@/models';

export function TypeName(type: TypeMaterial): string {
  switch (type) {
    case TypeMaterial.gaz:
      return 'Газобетон';
    case TypeMaterial.kirpich:
      return 'Кирпич';
    case TypeMaterial.keramzit:
      return 'Керамзитобетон';
    case TypeMaterial.pen:
      return 'Пенобетон';
  }

  return 'Тип не указан';
}

export function ComplectName(type: TypeConstruction): string {
  switch (type) {
    case TypeConstruction.box:
      return 'Коробка дома';
    case TypeConstruction.clearing:
      return 'Под чистовую отделку';
  }

  return 'Тип не указан';
}

export function FormatPrice(price: number = 0): string {
  price = Math.ceil(price) || 0;
  const strPrice = ('' + price).padStart(12, '0');
  const formatPrice: string[] = [];

  for (let i = 9; i > 0; i -= 3) {
    formatPrice.unshift(strPrice.substr(i, 3));
  }

  return formatPrice ? (formatPrice.join(' ').trim().replace(/^(0+\s?)+/, '') || '0') + ' ₽' : 'Цена не определена';
}
