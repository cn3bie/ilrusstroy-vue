import Vue from 'vue';
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';
import './vuetify.scss';

import '@fortawesome/fontawesome-free/css/all.css';

Vue.use(Vuetify, {
  theme: {
    primary: '#000000',
    secondary: '#9E9E9E',
    accent: '#000000',
    error: '#f44336',
    warning: '#ffeb3b',
    info: '#2196f3',
    success: '#4caf50',
  },
  customProperties: true,
  iconfont: 'md',
});
