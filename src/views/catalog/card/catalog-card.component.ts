
import {Component, Vue} from 'vue-property-decorator';
import {AxiosResponse} from 'axios';

import Cn3bieCharacteristic from '@/components/cn3bie/Cn3bieCharacteristic.vue';

import SectionMainInfo from '@/components/section/MainInfo.vue';
import SectionPlans from '@/components/section/Plans.vue';
import SectionFasad from '@/components/section/Fasad.vue';
import SectionTypeConstruction from '@/components/section/TypeConstruction.vue';
import SectionPhotootchet from '@/components/section/Photootchet.vue';
import SectionTalkAbout from '@/components/section/TalkAbout.vue';
import SectionOtherProjects from '@/components/section/OtherProjects.vue';
import SectionMaps from '@/components/section/Maps.vue';

import Settings from '@/assets/data/settings';

import {
  IProject,
  TypeMaterial,
  IPlan,
  IPhotootchet,
  IResponseProject,
  IResponseProjectGallery,
  IComplect,
  IImgWithName
} from '@/models';
import {
  ListAdditionals,
  ListTypeMaterial,
} from '@/assets/data/listProjects';

import projectsServer from '@/services/projects.service';
import {ActionType} from '@/store';

@Component({
  components: {
    Cn3bieCharacteristic,
    SectionMainInfo,
    SectionPlans,
    SectionFasad,
    SectionTypeConstruction,
    SectionPhotootchet,
    SectionTalkAbout,
    SectionOtherProjects,
    SectionMaps,
  },
})
export default class CardCatalog extends Vue {
  settings = Settings;

  get project(): IProject {
    return this.$store.state.currentProject.project;
  }

  get images(): string[] {
    return this.$store.state.currentProject.images;
  }

  set images(images: string[]) {
    this.$store.commit(ActionType.ChangeImagesSelectedProject, images);
  }

  get plans(): IPlan[] {
    return this.$store.state.currentProject.plans;
  }

  get fasads(): string[] {
    return this.$store.state.currentProject.fasads;
  }

  get currentMatterial() {
    return '' + ListTypeMaterial.indexOf(this.$store.state.catalog.currentMatterial);
  }

  set currentMatterial(index: string) {
    this.$store.commit(ActionType.ChangeMatterial, ListTypeMaterial[+index]);
  }

  get material() {
    return this.$store.state.catalog.currentMatterial;
  }

  get listTypeConstruction(): IComplect {
    return this.project.complect[this.material] || this.$store.state.settingsComplect[this.material];
  }

  additionals: IImgWithName[] = [];
  listPhotootchets: IPhotootchet[] = [];
  listTypeMaterial: TypeMaterial[] = ListTypeMaterial;

  mounted() {
    this.images = [this.project.img];

    projectsServer.getProjectById(this.$route.params.id)
      .then((res: AxiosResponse<IResponseProject>) => {
        Object.assign(this.project, res.data);
        if (this.$store.state.bookmark.ids[this.project.id]) {
          this.project.metadata.bookmark = true;
        }

        if (Array.isArray(res.data.images)) {
          this.images.length = 0;
          this.images.push(...res.data.images);
        }

        if (Array.isArray(res.data.plans)) {
          this.plans.length = 0;
          this.plans.push(...res.data.plans);
        }

        if (Array.isArray(res.data.fasads)) {
          this.fasads.length = 0;
          this.fasads.push(...res.data.fasads);
        }

        this.additionals.length = 0;
        Object.keys(res.data.additional)
          .forEach((key: string) => {
            const additional = ListAdditionals[res.data.additional[key]];
            if (additional) {
              this.additionals.push(additional);
            }
          });
      });

    projectsServer.getBuildListGalleryById(this.$route.params.id)
      .then((res: AxiosResponse<IResponseProjectGallery>) => {
        if (Array.isArray(res.data.data)) {
          this.listPhotootchets.length = 0;
          this.listPhotootchets.push(...res.data.data);
        }
      });
  }
}
