
import {Component, Vue} from 'vue-property-decorator';
import {Subject, from} from 'rxjs';
import {switchMap, map, filter} from 'rxjs/operators';

import Cn3bieGridLine from '@/components/cn3bie/Cn3bieGridLine.vue';
import Cn3bieListProjects from '@/components/cn3bie/Cn3bieListProjects.vue';
import Cn3bieFilterItem from '@/components/cn3bie/Cn3bieFilterItem.vue';
import Cn3bieReclama from '@/components/cn3bie/Cn3bieReclama.vue';

import FormSuitableProject from '@/components/form/SuitableProject.vue';
import SectionTalkAbout from '@/components/section/TalkAbout.vue';
import SectionMaps from '@/components/section/Maps.vue';

import Settings from '@/assets/data/settings';

import {
  IProject,
  ICn3bieSort,
  ICn3bieFilter,
  ISort,
  IQueryFilter,
  IResponsFilterItem,
  TypeTarget,
  TypeMaterial,
  TypeInput,
} from '@/models/projects.model';

import {
  ListFilters,
  ListTypeMaterial,
} from '@/assets/data/listProjects';

import projectsServer from '@/services/projects.service';
import {ActionType, IAction} from '@/store';
import {TypeName} from '@/filters';
import {Route} from 'vue-router';

const ListSort: ICn3bieSort[] = [{
  name: 'цена (возрастание)',
  value: {
    order: 'ASC',
    orderby: 'price',
  },
}, {
  name: 'цена (убывание)',
  value: {
    order: 'DESC',
    orderby: 'price',
  },
}, {
  name: 'площади (возрастание)',
  value: {
    order: 'ASC',
    orderby: 'area',
  },
}, {
  name: 'площади (убывание)',
  value: {
    order: 'DESC',
    orderby: 'area',
  },
}];

@Component({
  components: {
    Cn3bieGridLine,
    Cn3bieListProjects,
    Cn3bieFilterItem,
    Cn3bieReclama,
    FormSuitableProject,
    SectionTalkAbout,
    SectionMaps,
  },
})
export default class Catalog extends Vue {
  settings = Settings;
  title = '';
  listFilters: ICn3bieFilter[] = ListFilters;

  listSort: ICn3bieSort[] = ListSort;
  selectSort: ISort = ListSort[0].value;

  order = 'ASC';
  orderby = 'price';

  filters: {
    [id: string]: IQueryFilter,
  } = {};

  materials = ListTypeMaterial;

  moveBtnTop = 0;
  moveBtnShow = false;

  loading = false;
  loadAll = false;

  flow$: Subject<IAction> = new Subject<IAction>();

  counterSub = this.flow$.pipe(
    filter((action: IAction) => action.type === 'getCountByFilter'),
    switchMap(() => from(projectsServer.getCountProjects(1, -1, this.order, this.orderby, this.filters))),
    map(res => {
      this.filterCount = res.data.filterCount;
    }),
  ).subscribe();

  get totalCount() {
    return this.$store.state.catalog.totalCount;
  }

  set totalCount(count: number) {
    this.$store.commit(ActionType.TotalCount, count);
  }

  get moreCount() {
    return this.$store.state.catalog.moreCount;
  }

  set moreCount(count: number) {
    this.$store.commit(ActionType.MoreCount, count);
  }

  get filterCount() {
    return this.$store.state.catalog.filterCount;
  }

  set filterCount(count: number) {
    this.$store.commit(ActionType.FilterCount, count);
  }

  get size(): number {
    return this.loadAll ? -1 : this.$store.state.catalog.size;
  }

  get listProjects() {
    return this.$store.state.catalog.projects;
  }

  set listProjects(projects: IProject[]) {
    this.$store.commit(ActionType.LoadedProjects, {projects, size: this.size });
  }

  get gridAsLine() {
    return this.$store.state.catalog.asLine;
  }

  set gridAsLine(value: boolean) {
    this.$store.commit(ActionType.GridAsLine, value);
  }

  get name(): string {
    return this.getCommonName(this.moreCount);
  }

  get nameFilter(): string {
    return this.getCommonName(this.filterCount);
  }

  get currentMatterial(): TypeMaterial {
    return this.$store.state.catalog.currentMatterial;
  }

  constructor() {
    super();

    this.updateMeta = this.updateMeta.bind(this);
  }

  created() {
    this.updateMeta(this.$router.currentRoute, this.$router.currentRoute, () => {/** */});
    this.$router.beforeEach(this.updateMeta);
  }

  getTitle(material = ''): string {
    switch(material) {
      case TypeMaterial.gaz:
      case TypeMaterial.keramzit:
      case TypeMaterial.kirpich:
      case TypeMaterial.pen:
        return 'Проекты домов из ' + TypeName(material).toLowerCase() + 'а и цены';
      default:
        return 'Проекты домов и цены';
    }
  }

  updateMeta(to: Route, from: Route, next: () => void) {
    const key = to.path.replace(/catalog|\//gi, '')

    if (to.meta.title) {
      this.title = document.title = this.getTitle(key);

      switch(key) {
        case TypeMaterial.gaz:
        case TypeMaterial.keramzit:
        case TypeMaterial.kirpich:
        case TypeMaterial.pen:
          this.$store.commit(ActionType.ChangeMatterial, key);
          break;
        case 'onefloor':
          this.changeFilter({
            query: {
              name: 'floor',
              params: [
                {
                  name: 'floor',
                  value: 1,
                  type: TypeInput.checkbox
                }
              ]
            },
            event: undefined,
          });
          break;
        case 'onefloor_plus_mansandra':
          this.changeFilter({
            query: {
              name: 'floor',
              params: [
                {
                  name: 'floor',
                  value: 3,
                  type: TypeInput.checkbox
                }
              ]
            },
            event: undefined,
          });
          break;
        case 'twofloor':
          this.changeFilter({
            query: {
              name: 'floor',
              params: [
                {
                  name: 'floor',
                  value: 2,
                  type: TypeInput.checkbox
                }
              ]
            },
            event: undefined,
          });
          break;
        case 'twofloor_plus_mansandra':
          this.changeFilter({
            query: {
              name: 'floor',
              params: [
                {
                  name: 'floor',
                  value: 4,
                  type: TypeInput.checkbox
                }
              ]
            },
            event: undefined,
          });
          break;
        default:
          this.changeFilter({
            query: {
              name: 'floor',
              params: []
            },
            event: undefined,
          });
      }

      this.applyFilters();
    }

    next();
  }

  toggleGridAsLine() {
    this.gridAsLine = !this.gridAsLine;
  }

  change() {
    this.getProjects(this.selectSort.order, this.selectSort.orderby);
  }

  getAllProjects() {
    this.loadAll = true;
    this.getProjects();
  }

  changeFilter({query, event, target }: IResponsFilterItem) {
    this.filters[query.name] = query;

    if (query.name === 'material') {
      this.$store.dispatch(ActionType.ChangeMatterial, query.params[0].value);
    } else {
      if (event && target) {
        this.changeTopPositionFilterLabel(event, target);
      }

      this.flow$.next({ type: 'getCountByFilter' });
    }
  }

  applyFilters() {
    this.moveBtnShow = false;

    this.getProjects();
  }

  resetFilters(){
    this.filters = {};

    this.getProjects();
  }

  private getProjects(
    order = this.order,
    orderby = this.orderby,
  ) {
    this.order = order;
    this.orderby = orderby;

    this.loading = true;

    projectsServer.getProjects(1, this.size, order, orderby, this.filters)
      .then(res => {
        this.loading = false;

        this.listProjects = res.data.data;

        this.moreCount = this.filterCount - this.listProjects.length;
      });
  }

  private getCommonName(count: number) {
    const letter = parseInt(('' + count).split('').reverse()[0], 10) || 0;

    switch (true) {
      case 2 <= letter && letter <= 4:
        return 'а';
      case 5 <= letter && letter <= 9 || letter === 0:
        return 'ов';
    }

    return '';
  }

  private changeTopPositionFilterLabel(event: Event | undefined, target: {
    el: Element;
    type: TypeTarget;
  }) {
    if (event && event.target) {
      const el = event.target as HTMLElement;

      if (el !== null) {
        if (el.parentElement !== null) {
          if (target.type === TypeTarget.box) {
            if (el.parentElement.offsetParent !== null) {
              this.moveBtnTop = el.parentElement.offsetTop - 15;
            }
          } else {
            if (el.parentElement.parentElement !== null) {
             if (el.parentElement.parentElement.parentElement !== null) {
              if (el.parentElement.parentElement.parentElement.parentElement !== null) {
                this.moveBtnTop = el.parentElement.parentElement.parentElement.parentElement.offsetTop - 10;
              }
            }
           }
          }

          this.moveBtnShow = true;
          this.$set(this, 'moveBtnTop', this.moveBtnTop + 'px');
        }
      }
    }
  }
}
