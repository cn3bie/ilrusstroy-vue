import axios, { AxiosResponse} from 'axios';

import {environment} from '@/environments/environment';
import {IResponseModel, IReview} from '@/models';

const path = environment.api;

class ResponsesServer {
  /**
   * @param  {number=1} page
   * @param  {number=-1} size
   * @param  {string='ASC'} order
   * @param  {string='price'} orderby
   * @returns Promise
   */
  async getResponss(
    page: number = 1,
    size: number = -1,
  ): Promise<AxiosResponse<IResponseModel<IReview>>> {
    const params: string[] = [
      `pg=${page}`,
      `size=${size}`,
    ];

    return await axios.get(`${path}/responses/`, {params});
  }

  /**
   * @param  {string} id
   * @returns Promise
   */
  async getResponsById(id: string): Promise<AxiosResponse<IResponseModel<IReview>>> {
    return await axios.get(`${path}/responses/card/`, {params: {id}});
  }
}

export default new ResponsesServer();
