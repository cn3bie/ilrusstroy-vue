import axios, {AxiosResponse} from 'axios';

import {environment} from '@/environments/environment';
import {IResponseSettingsProject} from '@/models';

const path = environment.api;

class SettingsServer {
  async getProjectSettings(): Promise<AxiosResponse<IResponseSettingsProject>> {
    return await axios.get(`${path}/settings/`);
  }

  async mail(data: {[key: string]: string}): Promise<AxiosResponse<string>> {

    const form = new FormData();

    Object.keys(data).forEach(key => {
      form.append(key, data[key]);
    });

    return await axios.post(`${path}/mail/`, form);
  }
}

export default new SettingsServer();
