import axios, {AxiosResponse} from 'axios';

import {environment} from '@/environments/environment';
import {
  IResponseModel,
  IQueryFilter,
  IParam,
  IArticle,
  IUsefulArticle
} from '@/models';

const path = environment.api;

class NewsServer {
  /**
   * @param  {number=1} page
   * @param  {number=-1} size
   * @param  {string='ASC'} order
   * @param  {string='price'} orderby
   * @returns Promise
   */
  async getNews(
    page: number = 1,
    size: number = -1,
    order: string = 'ASC',
    orderby: string = 'price',
    filters?: {
      [id: string]: IQueryFilter,
    },
  ): Promise<AxiosResponse<IResponseModel<IUsefulArticle>>> {
    const params: string[] = [
      `pg=${page}`,
      `size=${size}`,
      `order=${order}`,
      `orderby=${orderby}`,
    ];
    if (filters) {
      Object.keys(filters).forEach((key: string) => {
        if (Array.isArray(filters[key].params)) {
          filters[key].params
            .forEach((item: IParam) => {
              params.push(item.name + '=' + item.value);
            });
        }
      });
    }

    return await axios.get(
      `${path}/news/?${params.join('&')}`,
    );
  }
  async getCountNews(
    page: number = 1,
    size: number = -1,
    order: string = 'ASC',
    orderby: string = 'price',
    filters: {
      [id: string]: IQueryFilter,
    },
  ): Promise<AxiosResponse<IResponseModel<IArticle>>> {
    const params: string[] = [
      'count=true',
      `pg=${page}`,
      `size=${size}`,
      `order=${order}`,
      `orderby=${orderby}`,
    ];
    Object.keys(filters).forEach((key: string) => {
      if (Array.isArray(filters[key].params)) {
        filters[key].params
          .forEach((item: IParam) => {
            params.push(item.name + '=' + item.value);
          });
      }
    });

    return await axios.get(
      `${path}/news/?${params.join('&')}`,
    );
  }

  async getPopularNews(): Promise<AxiosResponse<IResponseModel<IArticle>>> {
    return await axios.get(`${path}/popular/`);
  }

  /**
   * @param  {string} id
   * @returns Promise
   */
  async getArticleById(id: string): Promise<AxiosResponse<IUsefulArticle>> {
    return await axios.get(`${path}/news/card/?id=${id}`);
  }
}

export default new NewsServer();
