import axios, {AxiosResponse} from 'axios';

import {environment} from '@/environments/environment';
import {IResponseModel, IBetween, IResponseBookmark} from '@/models/response.model';
import {
  IProject,
  IQueryFilter,
  IParam,
  ICn3bieFilter,
  IGetParams,
  IResponseProjectGallery,
  IResponseProject,
  TypeInput,
  SimpleType,
} from '@/models';

const path = environment.api;

class ProjectsServer {
  /**
   * @param  {number=1} page
   * @param  {number=-1} size
   * @param  {string='ASC'} order
   * @param  {string='price'} orderby
   * @returns Promise
   */
  async getCountBuildProjects(): Promise<AxiosResponse<number>> {
    return await axios.get(`${path}/build/`, {params: {count: true}});
  }

  async getBuildProjects(pg: number = 1): Promise<AxiosResponse<IResponseModel<IProject>>> {
    return await axios.get(`${path}/build/`, {params: {pg}});
  }

  async getBuildedProjects(pg: number = 1): Promise<AxiosResponse<IResponseModel<IProject>>> {
    return await axios.get( `${path}/build/`, {params: {pg, builded: true}});
  }

  async getBuildProjectById(id: string): Promise<AxiosResponse<IResponseProject>> {
    return await axios.get( `${path}/build/card/`, {params: {id}});
  }

  async getProjects(
    pg: number = 1,
    size: number = -1,
    order: string = 'ASC',
    orderby: string = 'price',
    filters?: {
      [id: string]: IQueryFilter,
    },
  ): Promise<AxiosResponse<IResponseModel<IProject>>> {
    const params: IGetParams = this.parseParams({pg, size, order, orderby}, filters || {});

    return await axios.get(`${path}/catalog/`, {params});
  }

  async getCountProjects(
    pg: number = 1,
    size: number = -1,
    order: string = 'ASC',
    orderby: string = 'price',
    filters: {
      [id: string]: IQueryFilter,
    },
  ): Promise<AxiosResponse<IResponseModel<IProject>>> {
    const params: IGetParams = this.parseParams({
      pg, size, order, orderby,
      count: true,
    }, filters);


    return await axios.get(`${path}/catalog/`, {params});
  }

  async getSettingProjects(): Promise<AxiosResponse<IResponseModel<IProject>>> {
    return await axios.get(`${path}/catalog/`, {params: {settings: true}});
  }

  async getPopularProjects(): Promise<AxiosResponse<IResponseModel<IProject>>> {
    return await axios.get(`${path}/popular/`);
  }

  async getProjectById(id: string): Promise<AxiosResponse> {
    return await axios.get<IProject>(`${path}/catalog/card/`, {params: {id}});
  }

  async getProjectsByIds(ids: string[]): Promise<AxiosResponse<IResponseModel<IProject>>>  {
    return await axios.post(`${path}/catalog/listbyids/`, {ids });
  }

  async getBuildListGalleryById(id: string): Promise<AxiosResponse<IResponseProjectGallery>>  {
    return await axios.get(`${path}/build/listgallerybyid/`, {params: {id}});
  }

  async toggleBookmarkProjectById(id: string): Promise<AxiosResponse<IResponseBookmark>>  {
    return await axios.get(`${path}/`, {params: {bookmark: true, id}});
  }

  getBetween(settings: {[key: string]: IBetween; }, filters: ICn3bieFilter[]) {
    filters.forEach((filter: ICn3bieFilter) => {
      if (filter.slider) {
        filter.slider.min = settings[filter.name].min;
        filter.slider.max = settings[filter.name].max;
        filter.slider.range = [settings[filter.name].min, settings[filter.name].max];
      }
    });
  }

  private parseParams(params: IGetParams, filters: {
    [id: string]: IQueryFilter,
  }): IGetParams {
    Object.keys(filters).forEach((key: string) => {
      filters[key].params.forEach(input => {
        switch (input.type) {
          case TypeInput.radio:
            params[key] = input.value;
            break;
          case TypeInput.checkbox:
            params[key] = Array.isArray(params[key]) ? params[key] : [];
            (params[key] as SimpleType[]).push(input.value);
            break;
          default:
            params[input.name] = input.value;
        }
      });
    });

    return params;
  }
}

export default new ProjectsServer();
