# ilrusstroy-vue

## Необходимые программы на компе

* [NodeJS v^8.17.0](https://nodejs.org/en/)
* npm (идёт в комплекте с NodeJS)
* [yarn](https://yarnpkg.com/)

## Начало работ

```shell
yarn start
```

## Компиляция для production

```shell
yarn build
```
