FROM node:10.15.3-alpion

WORKDIR /vue

COPY ./dist .

ENTRYPOINT [ "node ./server.js" ]
